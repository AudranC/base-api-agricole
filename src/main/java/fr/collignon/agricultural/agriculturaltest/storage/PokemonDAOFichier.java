package fr.collignon.agricultural.agriculturaltest.storage;

import fr.collignon.agricultural.agriculturaltest.Pokemon;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "file", matchIfMissing = true)
public class PokemonDAOFichier implements  PokemonDAO {

    public Iterable<Pokemon> getMyPokemons() {
        System.out.println("Using fichier");
        File file = new File("pokemons.json");
        //Files.readAllLines(file.toPath());
        return new ArrayList<>();
    }

    @Override
    public void insererPokemon(Pokemon pokemon) {

    }
}
