package fr.collignon.agricultural.agriculturaltest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.collignon.agricultural.agriculturaltest.storage.PokemonDAO;
import fr.collignon.agricultural.agriculturaltest.storage.PokemonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class HelloController {
    private OkHttpClient client = new OkHttpClient();
    private Map<String, Pokemon> cachePokemon = new HashMap<>();
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private ServiceBidon serviceBidon;

    @Autowired
    private PokemonDAO pokemonDAO;

    public HelloController()  {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @GetMapping(value = "/mespokemons")
    public Iterable<Pokemon> mesPokemons() {
        return pokemonDAO.getMyPokemons();
    }

    @PostMapping(value = "/creerPokemon")
    public void creerPokemon() {
        Pokemon pokemon = new Pokemon();
        pokemon.setNom("Pokemon"+Math.random());
        Espece espece = new Espece();
        espece.setNom("Espece"+Math.random());
        pokemon.setEspece(espece);
        pokemonDAO.insererPokemon(pokemon);
    }

    @GetMapping(value = "/toto")
    public String index() {
        return "Hello World !";
    }

    @GetMapping("/espece/{pokemon}")
    public Pokemon getInfos(@PathVariable String pokemon) throws IOException {
        if ( cachePokemon.get(pokemon) != null)
        {
            return cachePokemon.get(pokemon);
        }

        Request request = new Request.Builder()
                .url("https://pokeapi.co/api/v2/pokemon/"+pokemon)
                .build();
        Response response = client.newCall(request).execute();

        Pokemon mappedPokemon = objectMapper.readValue(response.body().string(), Pokemon.class);
        cachePokemon.put(pokemon,mappedPokemon);

        return mappedPokemon;
    }

    public PokemonDAO getPokemonDAO() {
        return pokemonDAO;
    }

    public void setPokemonDAO(PokemonDAO pokemonDAO) {
        this.pokemonDAO = pokemonDAO;
    }
}
